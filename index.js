// console.log("Hey!")

// Exponent Operator
let num = 2
let getCube = (num)**3

// Template Literals
let ansCube =`The cube of ${num} is ${getCube}`
console.log(ansCube);

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];
console.log(`I live at ${address[0]} ${address[1]}, ${address[2]} ${address[3]} `);

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}
console.log(`${animal.name} was a ${animal.species}. He weighed at ${animal.weight} with a measurement of ${animal.measurement}.`);

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => {
	console.log(number);
	
})



// Javascript Classes

class Dog {
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
let dog1 = new Dog("Frankie","5","Miniature Dachshund");
let dog2 = new Dog("Jamies","2","Siberian Husky");

console.log(dog1);
console.log(dog2);